/*  SymbolTableTest.java */

package test;
import symbol.*;
import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class SymbolTableTest{

    @Test
    public void testSymbol(){
	Symbol s1 = Symbol.Symbol("Hello");
	Symbol s2 = Symbol.Symbol("Hello");
	Symbol s3 = Symbol.Symbol("world");
	assertSame("s1 and s2 should be equal for reference", s1, s2);
	assertNotSame("s1 and s3 should be not equal for reference", 
		      s1, s3);
	assertNotSame("s2 and s3 should be not equal for reference",
		      s2, s3);
	assertTrue("s1 can convert to string (Hello)", 
		   s1.toString().equals("Hello"));
	assertTrue("s2 can convert to string (Hello)", 
		   s2.toString().equals("Hello"));
	assertTrue("s3 can convert to string (world)", 
		   s3.toString().equals("world"));
    }

    public void testTable(){
	Table t = new Table();
	Symbol s1 = Symbol.Symbol("firstVar");
	Symbol s2 = Symbol.Symbol("secondVar");
	Symbol s3 = Symbol.Symbol("thirdVar");
	Symbol s4 = Symbol.Symbol("fourthVar");
	Symbol s5 = Symbol.Symbol("fifthVar");
	t.put(s1, 1);
	t.put(s2, 1);
	t.beginScope();
	t.put(s1, 2);
	t.put(s2, 2);
	t.put(s3, 1);
	assertEquals("s1 should be binded to 2", 2, t.get(s1));
	assertEquals("s2 should be binded to 2", 2, t.get(s2));
	assertEquals("s3 should be binded to 1", 1, t.get(s3));
	t.endScope();
	assertEquals("s1 should be binded to 1", 1, t.get(s1));
	assertEquals("s2 should be binded to 1", 1, t.get(s2));
	assertEquals("s3 has no binding now", null, t.get(s3));
	assertEquals("s4 has no binding now", null, t.get(s4));
    }

}
