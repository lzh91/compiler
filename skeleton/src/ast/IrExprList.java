/*  IrExprList.java */

package ast;
import java.util.*;

public class IrExprList{
    private Vector list;

    public IrExprList(){
	list = new Vector();
    }

    public void addElement(IrExpr e){
	list.addElement(e);
    }

    public IrExpr elementAt(int i){
	return (IrExpr)list.elementAt(i);
    }

    public int size(){
	return list.size();
    }
}