/*  IrAssignOp.java */

package ast;

public class IrAssignOp extends IR{
    public static final int ASSIGN = 0;
    public static final int ADDASSIGN = 1;
    public static final int MINUSASSIGN = 2;

    public int assignType;

    public IrAssignOp(String op){
	if (op.equals("="))
	    assignType = 0;
	else if (op.equals("+="))
	    assignType = 1;
	else if (op.equals("-="))
	    assignType = 2;
	else assignType = 3;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("assign operation: " + row + " " + col);
    }
}