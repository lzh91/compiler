/*  IrCharLiteral.java */

package ast;

public class IrCharLiteral extends IrLiteral{
    char c;

    public IrCharLiteral(String s){
	c = s.charAt(0);
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("char literal: " + row + " " + col);
    }
}