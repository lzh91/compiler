/*  IrType.java */

package ast;
import descriptor.*;

public class IrType extends IR{
    public IrType(String s){
	if (s.equals("int"))
	    type = Type.INTEGER;
	else if (s.equals("boolean"))
	    type = Type.BOOLEAN;
	else type = Type.ERROR;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i ++)
	    System.out.print(" ");
	if (type == Type.INTEGER)
	    System.out.println("type: int " + row + " " + col);
	else if (type == Type.BOOLEAN)
	    System.out.println("type: bool " + row + " " + col);
    }
}