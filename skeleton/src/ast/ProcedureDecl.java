/*  ProcedureDecl.java */

package ast;
import descriptor.*;

public class ProcedureDecl extends MethodDecl{
    public ProcedureDecl(IrId id, IrVarDeclList paramList, IrBlock block){
	this.id = id;
	this.paramList = paramList;
	this.block = block;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Procedure declaration: " + row + " " + col);
	id.printNode(indent + 1);
	for (int i = 0; i < paramList.size(); i++)
	    paramList.elementAt(i).printNode(indent+1);
	block.printNode(indent+1);
    }
}