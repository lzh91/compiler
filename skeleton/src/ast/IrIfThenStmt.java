/*  IrIfThenStmt.java */

package ast;

public class IrIfThenStmt extends IfStmt{
    public IrIfThenStmt(IrExpr c, IrBlock t){
	condition = c;
	thenStmt = t;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("If then statement: " + row + " " + col);
	condition.printNode(indent + 1);
	thenStmt.printNode(indent + 1);
    }
}