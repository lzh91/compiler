/* IrExMethodCall.java */

package ast;

public class IrExMethodCall extends IrMethodCall{
    IrStringLit exFunc;
    IrCalloutArgList args;

    public IrExMethodCall(IrStringLit e, IrCalloutArgList a){
	exFunc = e;
	args = a;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }
    
    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Call out method: " + row + " " + col);
	exFunc.printNode(indent + 1);
	for (int i = 0; i < args.size(); i++)
	    args.elementAt(i).printNode(indent + 1);
    }
}