/*  IrInMethodCall.java */

package ast;

public class IrInMethodCall extends IrMethodCall{
    IrMethodName mName;
    IrExprList exprs;

    public IrInMethodCall(IrMethodName m, IrExprList e){
	mName = m;
	exprs = e;
    }
    
    public String getMethodName(){
	return mName.id.toString();
    }
    
    public int accept(Visitor v){
	return v.visit(this);
    }
    
    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Call internal method: " + row + " " + col);
	mName.printNode(indent + 1);
	for (int i = 0; i < exprs.size(); i++)
	    exprs.elementAt(i).printNode(indent + 1);
    }
}