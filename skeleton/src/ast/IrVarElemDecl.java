/* IrVarElemDecl.java */

package ast;
import descriptor.*;

public abstract class IrVarElemDecl extends IR{
    protected IrId id;
    protected Descriptor descriptor;

    public String getVarName(){
	return id.toString();
    }
}