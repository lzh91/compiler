/*  IrEqOp.java */

package ast;

public class IrEqOp extends IrBinop{
    public static final int EQUAL = 0;
    public static final int NOTEQUAL = 1;

    public IrEqOp(String s){
	opClass = EQOP;
	if (s.equals("=="))
	    opType = 0;
	else if (s.equals("!="))
	    opType = 1;
	else 
	    opType = -1;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }
    
    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("equal operation: " + row + " " + col);	
    }
}