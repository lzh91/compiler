/*  IrDecLiteral.java */

package ast;

public class IrDecLiteral extends IrIntLiteral{
    public IrDecLiteral(String s){
	value = Integer.parseInt(s);
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("decimal integer: " + row + " " + col);
    }
}