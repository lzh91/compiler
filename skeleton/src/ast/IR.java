/*  IR.java */

package ast;

public abstract class IR{
    protected int row;
    protected int col;
    public int type;

    public void setRow(int row){
	this.row = row;
    }

    public void setCol(int col){
	this.col = col;
    }

    public int getRow(){
	return row;
    }

    public int getCol(){
	return col;
    }

    public abstract void printNode(int indent);
    public abstract int accept(Visitor v);
}