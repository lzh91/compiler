/*  IrIfThenELseStmt.java */

package ast;

public class IrIfThenElseStmt extends IfStmt{
    IrBlock elseStmt;

    public IrIfThenElseStmt(IrExpr c, IrBlock t, IrBlock e){
	condition = c;
	thenStmt = t;
	elseStmt = e;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("If then else statement: " + row + " " + col);
	condition.printNode(indent + 1);
	thenStmt.printNode(indent + 1);
	elseStmt.printNode(indent + 1);
    }
}