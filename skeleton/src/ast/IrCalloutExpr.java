/*  IrCalloutExpr.java */

package ast;

public class IrCalloutExpr extends IrCalloutArg{
    IrExpr e;

    public IrCalloutExpr(IrExpr e){
	this.e = e;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("call out expression: " + row + " " + col);
	e.printNode(indent + 1);
    }
}