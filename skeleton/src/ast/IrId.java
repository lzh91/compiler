/* IrId.java */

package ast;

public class IrId extends IR{
    private String identifier;

    public IrId(String s){
	identifier = s;
    }

    public String toString(){
	return identifier;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Id: " + identifier + " " + row + " " + col);
    }
}