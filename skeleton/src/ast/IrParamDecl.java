/*  IrParamDecl.java */

package ast;
import descriptor.*;

public class IrParamDecl extends IrVarElemDecl{
    public IrParamDecl(IrId i){
	id = i;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Parameter declaration: " + row + " " + col);
	id.printNode(indent+1);
    }
}