/*  IrLocalDecl.java */

package ast;
import descriptor.*;

public class IrLocalDecl extends IrVarElemDecl{
    public IrLocalDecl(IrId i){
	id = i;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Local Declaration: " + getRow() + " " + getCol());
	id.printNode(indent+1);
    }
}