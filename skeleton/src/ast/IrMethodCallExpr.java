/*  IrMethodCallExpr.java */

package ast;

public class IrMethodCallExpr extends IrExpr{
    IrMethodCall m; 

    public IrMethodCallExpr(IrMethodCall m){
	this.m = m;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("method call expression: " + row + " " + col);
	m.printNode(indent + 1);
    }
}