/*  IrMethodName.java */

package ast;

public class IrMethodName extends IR{
    IrId id;

    public IrMethodName(IrId id){
	this.id = id;
    }
    
    public int accept(Visitor v){
	return v.visit(this);
    }
    
    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("method name: " + row + " " + col);
	id.printNode(indent + 1);
    }
}