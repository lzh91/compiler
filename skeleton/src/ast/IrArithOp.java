/*  IrArithOp.java */

package ast;

public class IrArithOp extends IrBinop{
    public static final int ADD = 0;
    public static final int MINUS = 1;
    public static final int MULTIPLY = 2;
    public static final int DIVIDE = 3;
    public static final int MOD = 4;

    public IrArithOp(String s){
	opClass = ARITHOP;
	if (s.equals("+"))
	    opType = 0;
	else if (s.equals("-"))
	    opType = 1;
	else if (s.equals("*"))
	    opType = 2;
	else if (s.equals("/"))
	    opType = 3;
	else if (s.equals("%"))
	    opType = 4;
	else opType = -1;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("arithmetic operation: " + row + " " + col);
    }
}