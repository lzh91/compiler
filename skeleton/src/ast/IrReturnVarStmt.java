/* IrReturnVarStmt.java */

package ast;

public class IrReturnVarStmt extends IrReturnStmt{
    IrExpr returnExpr;

    public IrReturnVarStmt(IrExpr e){
	returnExpr = e;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("return statement: " + row + " " + col);
	returnExpr.printNode(indent + 1);
    }
}