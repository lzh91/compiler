/*  IrUminusExpr.java */

package ast;

public class IrUminusExpr extends IrExpr{
    IrExpr e;
    public int countOfMinus;

    public IrUminusExpr(int c, IrExpr e){
	this.e = e;
	countOfMinus = c;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("uminus expression: " + row + " " + col);
	e.printNode(indent + 1);
    }
}