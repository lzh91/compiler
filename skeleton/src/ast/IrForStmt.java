/*  IrForStmt.java */

package ast;

public class IrForStmt extends IrStmt{
    IrId id;
    IrExpr initExpr;
    IrExpr condition;
    IrBlock block;

    public IrForStmt(IrId id, IrExpr initExpr, IrExpr condition, 
		     IrBlock block){
	this.id = id;
	this.initExpr = initExpr;
	this.condition = condition;
	this.block = block;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i ++)
	    System.out.print(" ");
	System.out.println("for statement: " + row + " " + col);
	id.printNode(indent + 1);
	initExpr.printNode(indent + 1);
	condition.printNode(indent + 1);
	block.printNode(indent + 1);
    }
}