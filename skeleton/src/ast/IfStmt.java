/*  IfStmt.java */

package ast;

public abstract class IfStmt extends IrStmt{
    protected IrExpr condition;
    protected IrBlock thenStmt;
}