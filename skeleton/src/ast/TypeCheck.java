/*  TypeCheck.java */

package ast;
import descriptor.*;
import symbol.*;
import environment.*;
import java.util.*;

/**
 *  Do semantic analysis on the AST of decaf programme.
 *  visit pattern is applied to check each class instance
 *  the return value of the visit is the type of the 
 *  corresponding class.
 *
 *  @author liuzhuanghua
 *  @date   17112013 
 **/

public class TypeCheck implements Visitor{
    private Table symbolTable;

    /**
     *  check each variable declaration in the class scope to ensure no identifier
     *  is defined twice.
     **/

    private void checkClassFields(IrClassDecl classDecl, Hashtable<Symbol, Object> idTable){
	for (int i = 0; i < classDecl.fields.size(); i++){
	    IrVarDecl varDecl = classDecl.fields.elementAt(i);
	    if (varDecl.accept(this) == Type.ERROR){
		classDecl.type = Type.ERROR;
		return;
	    }	
	    for (int j = 0; j < varDecl.vars.size(); j++){
		IrVarElemDecl varElem = varDecl.vars.elementAt(j);
		String varName = varElem.getVarName();
		if(idTable.get(Symbol.Symbol(varName)) == null){
		    idTable.put(Symbol.Symbol(varName), new Object());
		    ((ClassDescriptor)classDecl.classDescriptor).
			addField(Symbol.Symbol(varName), varElem.type);
		} else{
		    System.out.print("line: " + varElem.getRow() + ", column:" + varElem.getCol());
		    System.out.println("error: identifier " + varName + " has been declared before in the same scope.");
		    classDecl.type = Type.ERROR;
		}
	    }
	}
    }

    /**
     *  check each method declartion in the class declaration. It also checks: (1)
     *  No duplicate identifier declared in the same scope. (2) contain "main" method
     *  declaration with no parameters.
     **/

    private void checkClassMethods(IrClassDecl classDecl, Hashtable<Symbol, Object> idTable){
	boolean containMain = false;
	for (int i = 0; i < classDecl.methods.size(); i++){
	    MethodDecl method = classDecl.methods.elementAt(i);
	    if (method.accept(this) == Type.ERROR){
		classDecl.type = Type.ERROR;
	    }
	    String methodName = method.getMethodName();
	    if (Symbol.Symbol(methodName) == Symbol.Symbol("main")){
		containMain = true;
		if (((MethodDescriptor)method.methodDescriptor).paramSize() != 0){
		    System.out.print("line: " + method.getRow() + ", column: " + method.getCol());
		    System.out.println("error: main method must be declared without parameters.");
		    classDecl.type = Type.ERROR;
		}
	    }
	    
	    if (idTable.get(Symbol.Symbol(methodName)) == null){
		idTable.put(Symbol.Symbol(methodName), new Object());
		((ClassDescriptor)classDecl.classDescriptor).
		    addMethodElement(Symbol.Symbol(methodName), method.type);
	    } else {
		System.out.print("Line: " + method.getRow() + ", column: " + method.getCol());
		System.out.println("error: identifier " + methodName + " has been declared before in the same scope.");
		classDecl.type = Type.ERROR;
	    }
	}

	if (!containMain){
	    System.out.print("Line: " + classDecl.getRow() + ", column:" + classDecl.getCol());
	    System.out.println("error: this class contains no main method.");
	}
    }

    /**
     *  visit all the fields and methods of the class. If one of the field definition
     *  or method definition is error, then the type of class set to be error.
     *  In addition, this method check whether there is duplicate identifier defined
     *  and whether the class contain main method which contains no parameters.
     *  otherwise the type of the class declaration is set to be void.
     *
     *  @param classDecl is the class declaration node of the AST.
     *  @return type of the classDecl.
     **/

    public int visit(IrClassDecl classDecl){
	symbolTable = new Table();
	Hashtable<Symbol, Object> idTable = new Hashtable<Symbol, Object>();
	classDecl.type = Type.VOID;
	classDecl.classDescriptor = new ClassDescriptor("Program");

	symbolTable.put(Symbol.Symbol("Program"), classDecl.classDescriptor);
	symbolTable.beginScope();
	checkClassFields(classDecl, idTable);
	checkClassMethods(classDecl, idTable);
	symbolTable.endScope();


	return classDecl.type;
    }

    public int visit(ArrayFieldDecl arrDecl){
	if (arrDecl.type == Type.INTEGER){
	    arrDecl.type = Type.INTARRAY;
	} else if (arrDecl.type == Type.BOOLEAN){
	    arrDecl.type = Type.BOOLARRAY;
	} else {
	    arrDecl.type = Type.ERROR;
	}
	
	arrDecl.descriptor = new FieldDescriptor(arrDecl.id.toString(), arrDecl.type);
	symbolTable.put(Symbol.Symbol(arrDecl.id.toString()), arrDecl.descriptor);
	if (arrDecl.arrSize.value <= 0){
	    arrDecl.type = Type.ERROR;
	    System.out.print("line: " + arrDecl.getRow() + ", column: " + arrDecl.getCol());
	    System.out.println("array field " + arrDecl.id.toString() + "must has size > 0, " + 
			       "but size " + arrDecl.arrSize.value + " <= 0.");
	}
	
	return arrDecl.type;
    }

    public int visit(IrType t){
	return t.type;
    }

    public int visit(IrId id){
	id.type = Type.VOID;
	return id.type;
    }

    private void accessBlockVars(IrBlock block, Hashtable<Symbol, Object> idTable){
	for (int i = 0; i < block.vars.size(); i++){
	    IrVarDecl localVars = block.vars.elementAt(i);
	    if (localVars.accept(this) == Type.ERROR){
		block.type = Type.ERROR;
	    }
	    for (int j = 0; j < localVars.vars.size(); j++){
		IrVarElemDecl local = localVars.vars.elementAt(j);
		if (idTable.get(Symbol.Symbol(local.getVarName())) == null){
		    idTable.put(Symbol.Symbol(local.getVarName()), new Object());
		} else {
		    System.out.print("line: " + local.getRow() + ", column: " + local.getCol());
		    System.out.println("error: local varible " + local.getVarName() + "has been declared before.");
		    block.type = Type.ERROR;
		}
	    }
	}
    }

    private void accessBlockStmts(IrBlock block, Hashtable<Symbol, Object> idTable){
	for (int i = 0; i < block.statements.size(); i++){
	    IrStmt stmt = block.statements.elementAt(i);
	    if (stmt.accept(this) == Type.ERROR){
		block.type = Type.ERROR;
	    }	    
	}
    }

    public int visit(IrBlock block){
	Hashtable<Symbol, Object> idTable = new Hashtable<Symbol, Object>();
	block.type = Type.VOID;
	accessBlockVars(block, idTable);
	accessBlockStmts(block, idTable);
	return block.type;
    }

    public int visit(FunctionDecl funcDecl){
	funcDecl.type = funcDecl.irType.type;
	accessMethod(funcDecl);
	return funcDecl.type;
    }
    private void accessIfStmt(IfStmt ifStmt){
	if (ifStmt.condition.accept(this) != Type.BOOLEAN){
	    ifStmt.type = Type.ERROR;
	    System.out.print("line: " + ifStmt.condition.getRow() + 
			     "column: " + ifStmt.condition.getCol());
	    System.out.println("error: the condition of if statement should be of type boolean");
	}
	symbolTable.beginScope();
	if (ifStmt.thenStmt.accept(this) == Type.ERROR)
	    ifStmt.type = Type.ERROR;
	symbolTable.endScope();
    }

    public int visit(IrIfThenStmt ifStmt){
	ifStmt.type = Type.VOID;
	accessIfStmt(ifStmt);
	return ifStmt.type;
    }

    public int visit(IrIfThenElseStmt ifStmt){
	ifStmt.type = Type.VOID;
	accessIfStmt(ifStmt);
	symbolTable.beginScope();
	if (ifStmt.elseStmt.accept(this) == Type.ERROR)
	    ifStmt.type = Type.ERROR;
	symbolTable.endScope();
	return ifStmt.type;
    }

    public int visit(IrArithOp arithOp){
	arithOp.type = Type.VOID;
	return arithOp.type;
    }

    public int visit(IrArrElemLocation arrElem){
	Descriptor varDescriptor = symbolTable.get(Symbol.Symbol(arrElem.id.toString()));
	if (!existVar(varDescriptor, arrElem)){
	    return arrElem.type;
	}
	if (varDescriptor.descripType != Descriptor.FIELD){
	    arrElem.type = Type.ERROR;
	    return arrElem.type;
	}
	arrElem.type = ((FieldDescriptor)varDescriptor).getType();
	if (arrElem.type != Type.INTARRAY && arrElem.type != Type.BOOLARRAY)
	    arrElem.type = Type.ERROR;

	if (arrElem.type == Type.INTARRAY){
	    arrElem.type = Type.INTEGER;
	} else if (arrElem.type == Type.BOOLARRAY){
	    arrElem.type = Type.BOOLEAN;
	}

	if (arrElem.index.accept(this) != Type.INTEGER){
	    arrElem.type = Type.ERROR;
	    System.out.print("line: " + arrElem.index.getRow() + 
			     "column: " + arrElem.index.getCol());
	    System.out.println("error: index should be integer.");
	}

	return arrElem.type;
    }

    public int visit(IrAssignOp assignOp){
	assignOp.type = Type.VOID;
	return assignOp.type;
    }

    public int visit(IrAssignStmt assignStmt){
	assignStmt.type = Type.VOID;
	if (assignStmt.lhs.accept(this) == Type.ERROR){	   
	    assignStmt.type = Type.ERROR;
	    return assignStmt.type;
	}

	if (assignStmt.rhs.accept(this) == Type.ERROR){
	    assignStmt.type = Type.ERROR;
	    return assignStmt.type;
	}

	if (assignStmt.lhs.type != assignStmt.rhs.type){
	    assignStmt.type = Type.ERROR;
	    System.out.print("line: " + assignStmt.getRow() + 
			     "column: " + assignStmt.getCol());
	    System.out.println("error: the type of lhs and the rhs of the assignment should be the same.");
	}
	if (assignStmt.op.assignType == 1 || assignStmt.op.assignType == 2){
	    if (assignStmt.lhs.type != Type.INTEGER || assignStmt.rhs.type != Type.INTEGER){
		assignStmt.type = Type.ERROR;
		System.out.print("line: " + assignStmt.getRow() + 
				 "column: " + assignStmt.getCol());
		System.out.println("error: the type of both side of the plus/minus assignment should be integer");
	    }
	}	
	return assignStmt.type;
    }

    public int visit(IrBinopExpr binopExpr){
	if (binopExpr.e1.accept(this) == Type.ERROR){
	    binopExpr.type = Type.ERROR;
	}
	if (binopExpr.e2.accept(this) == Type.ERROR){
	    binopExpr.type = Type.ERROR;
	}
	if (binopExpr.op.opClass == IrBinop.ARITHOP || binopExpr.op.opClass == IrBinop.RELOP){
	    if (binopExpr.e1.type == Type.INTEGER && binopExpr.e2.type == Type.INTEGER){
		if (binopExpr.op.opClass == IrBinop.ARITHOP)
		    binopExpr.type = Type.INTEGER;
		else binopExpr.type = Type.BOOLEAN;		
	    }
	    else {
		binopExpr.type = Type.ERROR;
		System.out.print("line: " + binopExpr.getRow() + 
				 ", column: " + binopExpr.getCol());
		System.out.println("error: the both side of the expression should be integer");
	    }
	} else if (binopExpr.op.opClass == IrBinop.EQOP){
	    if (binopExpr.e1.type == binopExpr.e2.type){
		if (binopExpr.e1.type == Type.INTEGER || binopExpr.e1.type == Type.BOOLEAN){
		    binopExpr.type = Type.BOOLEAN;
		}
	    } else {
		binopExpr.type = Type.ERROR;
		System.out.print("line: " + binopExpr.getRow() + 
				 ", column: " + binopExpr.getCol());
		System.out.println("error: both side of the expression should be the same type, either integer of boolean.");
	    }
		
	} else if (binopExpr.op.opClass == IrBinop.CONDOP){
	    if (binopExpr.e1.type == Type.BOOLEAN && binopExpr.e2.type == Type.BOOLEAN){
		binopExpr.type = Type.BOOLEAN;
	    } else {
		binopExpr.type = Type.ERROR;
		System.out.print("line: " + binopExpr.getRow() + 
				 ", column: " + binopExpr.getCol());
		System.out.println("error: the both side of the expression should be boolean.");
	    }
	} else{
	    binopExpr.type = Type.ERROR;
	}

	return binopExpr.type;
    }

    public int visit(IrBinop binop){
	binop.type = Type.VOID;
	return binop.type;
    }

    public int visit(IrBlockStmt blockStmt){
	blockStmt.type = Type.VOID;
	symbolTable.beginScope();
	if (blockStmt.block.accept(this) == Type.ERROR)
	    blockStmt.type = Type.ERROR;
	symbolTable.endScope();
	return blockStmt.type;
    }

    public int visit(IrBoolLiteral boolLit){
	boolLit.type = Type.BOOLEAN;
	return boolLit.type;
    }

    public int visit(IrBreakStmt breakStmt){
	breakStmt.type = Type.VOID;
	if (!Scope.getIsLoop()){
	    breakStmt.type = Type.ERROR;
	    System.out.print("line: " + breakStmt.getRow() + 
			     "column: " + breakStmt.getCol());
	    System.out.println("break statement should be in the loop.");
	}
	return breakStmt.type;
    }

    public int visit(IrCalloutExpr callExpr){
	callExpr.type = callExpr.e.accept(this);	
	return callExpr.type;
    }

    public int visit(IrCalloutString callStr){
	callStr.type = callStr.s.accept(this);
	return callStr.type;
    }

    public int visit(IrStringLit strLit){
	strLit.type = Type.STRING;
	return strLit.type;
    }

    public int visit(IrCharLiteral charLit){
	charLit.type = Type.CHAR;
	return charLit.type;
    }

    public int visit(IrCondOp condOp){
	condOp.type = Type.VOID;
	return condOp.type;
    }

    public int visit(IrContinueStmt contStmt){
	contStmt.type = Type.VOID;
	if (!Scope.getIsLoop()){
	    contStmt.type = Type.ERROR;
	    System.out.print("line: " + contStmt.getRow() + 
			     "column: " + contStmt.getCol());
	    System.out.println("error: the continue statement should be in the loop.");
	}
	return contStmt.type;
    }

    public int visit(IrDecLiteral decLit){
	decLit.type = Type.INTEGER;
	return Type.INTEGER;
    }

    public int visit(IrEqOp eqOp){
	eqOp.type = Type.VOID;
	return eqOp.type;
    }

    public int visit(IrExMethodCall exCallout){
	exCallout.type = Type.INTEGER;
	for (int i = 0; i < exCallout.args.size(); i++){
	    IrCalloutArg arg = exCallout.args.elementAt(i);
	    if (arg.accept(this) == Type.ERROR){
		exCallout.type = Type.ERROR;
	    }
	}
	return exCallout.type;
    }

    public int visit(IrForStmt forStmt){
	forStmt.type = Type.VOID;
	symbolTable.beginScope();
	Scope.setIsLoop(true);
	symbolTable.put(Symbol.Symbol(forStmt.id.toString()), 
			new LocalDescriptor(forStmt.id.toString(), Type.INTEGER));
	if (forStmt.initExpr.accept(this) != Type.INTEGER){
	    forStmt.type = Type.ERROR;
	    System.out.print("line: " + forStmt.initExpr.getRow() + 
			     "column: " + forStmt.initExpr.getCol());
	    System.out.println("error: the initial expression for for statement should be of type int.");
	} 
	if (forStmt.condition.accept(this) != Type.INTEGER){
	    forStmt.type = Type.ERROR;
	    System.out.print("line: " + forStmt.condition.getRow() + 
			     "column: " + forStmt.condition.getCol());
	    System.out.println("error: the final expression for for statement should be of type int.");
	}
	if (forStmt.block.accept(this) == Type.ERROR){
	    forStmt.type = Type.ERROR;
	}
	Scope.setIsLoop(false);
	symbolTable.endScope();
	return forStmt.type;
    }

    public int visit(IrHexLiteral hexLit){
	hexLit.type = Type.INTEGER;
	return hexLit.type;
    }

    public int visit(IrInMethodCall inCallout){
	inCallout.type = Type.VOID;
	Descriptor methodDescriptor = 
	    (Descriptor)symbolTable.get(Symbol.Symbol(inCallout.getMethodName()));	
	if (!methodExist(methodDescriptor, inCallout)){	   
	    return inCallout.type;
	}

	inCallout.type = ((MethodDescriptor)methodDescriptor).getReturnType();
	if (((MethodDescriptor)methodDescriptor).paramSize() != inCallout.exprs.size()){
	    inCallout.type = Type.ERROR;
	    System.out.print("line: " + inCallout.getRow() + 
			     "column: " + inCallout.getCol());
	    System.out.println("error: the number of parameters doesn't match to the number of actual expressions.");
	} else{
	    for (int i = 0; i < inCallout.exprs.size(); i++){
		IrExpr expr = inCallout.exprs.elementAt(i);
		if (expr.accept(this) == Type.ERROR)
		    inCallout.type = Type.ERROR;
		int paramType = ((MethodDescriptor)methodDescriptor).paramElementAt(i).type;
		if (expr.type != paramType){
		    inCallout.type = Type.ERROR;
		    System.out.print("line: " + expr.getRow() +
				     "column: " + expr.getCol());
		    System.out.println("error: the parameter type doesn't match the actual expression type.");
		}
	    }
	}
	return inCallout.type;
    }

    public int visit(IrLiteralExpr litExpr){
	litExpr.type = litExpr.lit.accept(this);
	return litExpr.type;
    }

    public int visit(IrLocationExpr locExpr){
	locExpr.type = locExpr.loc.accept(this);
	return locExpr.type;
    }

    public int visit(IrMethodCallExpr callExpr){
	callExpr.type = callExpr.m.accept(this);
	if (callExpr.type == Type.VOID){
	    callExpr.type = Type.ERROR;
	    System.out.print("line: " + callExpr.getRow() + 
			     "column: " + callExpr.getCol());
	    System.out.println("error: the method call expression should return a value.");
	}
	return callExpr.type;
    }

    public int visit(IrMethodCallStmt callStmt){
	callStmt.type = Type.VOID;
	return callStmt.type;
    }

    public int visit(IrNotExpr notExpr){
	notExpr.type = notExpr.e.accept(this);
	if (notExpr.countOfNot > 0){
	    if (notExpr.e.type != Type.BOOLEAN){
		notExpr.type = Type.ERROR;
		System.out.print("line: " + notExpr.e.getRow() + 
				 "column: " + notExpr.e.getCol());
		System.out.println("error: operand of logical not must be of type boolean.");
	    }
	}
	return notExpr.type;
    }

    public int visit(IrParenExpr expr){
	expr.type = expr.e.accept(this);
	return expr.type;
    }

    public int visit(IrRelOp relOp){
	relOp.type = Type.VOID;
	return relOp.type;
    }

    private boolean existVar(Descriptor varDescriptor, IR var){
	if (varDescriptor == null){
	    var.type = Type.ERROR;
	    System.out.print("line: " + var.getRow() + 
			     "column: " + var.getCol());
	    System.out.println("error: the variable doesn't exist.");
	    return false;
	}
	return true;
    }

    public int visit(IrVarLocation varLoc){
	Descriptor varDescriptor = symbolTable.get(Symbol.Symbol(varLoc.id.toString()));
	if (!existVar(varDescriptor, varLoc)){
	    varLoc.type = Type.ERROR;
	    return varLoc.type;
	}
	if (varDescriptor.descripType == Descriptor.METHOD || 
	    varDescriptor.descripType == Descriptor.CLASS){
	    varLoc.type = Type.ERROR;
	    return varLoc.type;
	}
	if (varDescriptor.descripType == Descriptor.LOCAL){
	    varLoc.type = ((LocalDescriptor)varDescriptor).getType();
	} else if (varDescriptor.descripType == Descriptor.PARAM){
	    varLoc.type = ((ParamDescriptor)varDescriptor).getType();
	} else if (varDescriptor.descripType == Descriptor.FIELD){
	    varLoc.type = ((FieldDescriptor)varDescriptor).getType();
	}

	if (varLoc.type != Type.INTEGER && varLoc.type != Type.BOOLEAN){
	    varLoc.type = Type.ERROR;	
	    System.out.print("line: " + varLoc.getRow() +
			     "column: " + varLoc.getCol());
	    System.out.println(" error: the variable should be either integer or boolean.");
	}
	return varLoc.type;
    }

    private void accessParams(MethodDecl methodDecl, Hashtable<Symbol, Object> idTable){
	for (int i = 0; i < methodDecl.paramList.size(); i++){
	    IrVarDecl param = methodDecl.paramList.elementAt(i);
	    if (param.accept(this) == Type.ERROR)
		methodDecl.type = Type.ERROR;
	    String paramName = param.vars.elementAt(0).getVarName();
	    int paramType = param.vars.elementAt(0).type;
	    if (idTable.get(Symbol.Symbol(paramName)) == null){
		idTable.put(Symbol.Symbol(paramName), paramType);
		((MethodDescriptor)methodDecl.methodDescriptor).
		    addParam(Symbol.Symbol(paramName), paramType);
	    } else {
		System.out.print("Line: " + param.getRow() + ", Column:" + param.getCol());
		System.out.println("error: parameter " + paramName + "has been declared");
		methodDecl.type = Type.ERROR;
	    }
	}
    }
    
    private void accessLocalVars(MethodDecl methodDecl, Hashtable<Symbol, Object> idTable){
	if (methodDecl.block.accept(this) == Type.ERROR)
	    methodDecl.type = Type.ERROR;

	// do stuff on the variable declaration in the method	
	for (int i = 0; i < methodDecl.block.vars.size(); i++){
	    IrVarDecl local = methodDecl.block.vars.elementAt(i);
	    for (int j = 0; j < local.vars.size(); j++){
		String localVarName = local.vars.elementAt(j).getVarName();
		if (idTable.get(Symbol.Symbol(localVarName)) != null){		  
		    IrVarElemDecl localVar = local.vars.elementAt(j);
		    System.out.print("line: " + localVar.getRow() + ", column: " + localVar.getCol());
		    System.out.println("error: local variable " + localVar.getVarName() + "has been declared before.");
		    methodDecl.type = Type.ERROR;
		}
	    }
	}	
    }
    
    private void accessMethod(MethodDecl methodDecl){
	Hashtable<Symbol, Object> idTable = new Hashtable<Symbol, Object>();
	methodDecl.methodDescriptor = new MethodDescriptor(methodDecl.getMethodName(), methodDecl.type);
	symbolTable.put(Symbol.Symbol(methodDecl.getMethodName()), methodDecl.methodDescriptor);
	Scope.setMethodName(methodDecl.getMethodName());
	symbolTable.beginScope();
	//  do stuff on params
	accessParams(methodDecl, idTable);
	accessLocalVars(methodDecl, idTable);
	symbolTable.endScope();
	Scope.setMethodName("");
    }

    public int visit(ProcedureDecl methodDecl){
	methodDecl.type = Type.VOID;
	accessMethod(methodDecl);
	return methodDecl.type;
    }

    public int visit(VarFieldDecl fieldDecl){
	fieldDecl.descriptor = new FieldDescriptor(fieldDecl.getVarName(), fieldDecl.type);
	symbolTable.put(Symbol.Symbol(fieldDecl.getVarName()), fieldDecl.descriptor);
	return fieldDecl.type;
    }

    public int visit(IrUminusExpr expr){
	expr.type = expr.e.accept(this);
	if (expr.countOfMinus > 0){
	    if (expr.e.type != Type.INTEGER){
		expr.type = Type.ERROR;
		System.out.print("line: " + expr.e.getRow() +
				 "column: " + expr.e.getCol());
		System.out.println("error: the type of expression of the uniminus should be of type integer.");
	    }	    
	}	    
	return expr.type;
    }

    public int visit(IrMethodName mName){
	mName.type = mName.id.accept(this);
	return mName.type;
    }

    public int visit(IrLocalDecl localDecl){
	String localName = localDecl.getVarName();
	localDecl.descriptor = new LocalDescriptor(localName, localDecl.type);
	symbolTable.put(Symbol.Symbol(localName), localDecl.descriptor);
	return localDecl.type;
    }

    public int visit(IrParamDecl paramDecl){
	String paramName = paramDecl.getVarName();
	paramDecl.descriptor = new ParamDescriptor(paramName, paramDecl.type);
	symbolTable.put(Symbol.Symbol(paramName), paramDecl.descriptor);
	return paramDecl.type;
    }

    private boolean methodExist(Descriptor methodDescriptor, IR irStmt){
	if (methodDescriptor == null){
	    irStmt.type = Type.ERROR;
	    System.out.print("line: " + irStmt.getRow() + 
			     "column: " + irStmt.getCol());
	    System.out.println("error: the method doesn't exist.");
	    irStmt.type = Type.ERROR;
	    return false;
	}

	if (methodDescriptor.descripType != Descriptor.METHOD){
	    irStmt.type = Type.ERROR;
	    return false;
	}
	return true;
    }

    public int visit(IrReturnVarStmt returnStmt){
	returnStmt.type = Type.VOID;
	if (returnStmt.returnExpr.accept(this) == Type.ERROR)
	    returnStmt.type = Type.ERROR;
	Descriptor methodDescriptor = 
	    symbolTable.get(Symbol.Symbol(Scope.getMethodName()));
	if (!methodExist(methodDescriptor, returnStmt))
	    return returnStmt.type;

	if (((MethodDescriptor)methodDescriptor).getReturnType() != returnStmt.returnExpr.type){
	    returnStmt.type = Type.ERROR;
	    System.out.print("line: " + returnStmt.returnExpr.getRow() + 
			     " column:" + returnStmt.returnExpr.getCol());
	    System.out.println("error: type of return expression does not equal to the type of method");
	}
	return returnStmt.type;
    }

    public int visit(IrReturnVoidStmt returnStmt){
	returnStmt.type = Type.VOID;
	Descriptor methodDescriptor = 
	    symbolTable.get(Symbol.Symbol(Scope.getMethodName()));
	if (!methodExist(methodDescriptor, returnStmt))
	    return returnStmt.type;

	if (((MethodDescriptor)methodDescriptor).getReturnType() != Type.VOID){
	    returnStmt.type = Type.ERROR;
	    System.out.print("line: " + returnStmt.getRow() + 
			     " column:" + returnStmt.getCol());
	    System.out.println("error: return expression should not be void");
	}
	return returnStmt.type;
    }

    public int visit(IrVarDecl varDecl){
	varDecl.type = varDecl.irType.type;

	for (int i = 0; i < varDecl.vars.size(); i++){
	    IrVarElemDecl var = varDecl.vars.elementAt(i);
	    var.type = varDecl.irType.type;
	    if(var.accept(this) == Type.ERROR){
		varDecl.type = Type.ERROR;
	    }
	}
	return varDecl.type;
    }
}