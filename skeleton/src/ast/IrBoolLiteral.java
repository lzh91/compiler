/*  IrBoolLiteral.java */

package ast;

public class IrBoolLiteral extends IrLiteral{
    boolean flag;
    
    public IrBoolLiteral(String s){
	if (s.equals("true"))
	    flag = true;
	else flag = false;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Bool literal: " + row + " " + col);
    }
}