/*  IrStmtList.java */

package ast;
import java.util.*;

public class IrStmtList{
    private Vector list;

    public IrStmtList(){
	list = new Vector();
    }

    public void addElement(IrStmt s){
	list.addElement(s);
    }

    public IrStmt elementAt(int i){
	return (IrStmt)list.elementAt(i);
    }

    public int size(){
	return list.size();
    }
}