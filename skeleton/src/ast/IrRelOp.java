/*  IrRelOp.java */
package ast;

public class IrRelOp extends IrBinop{
    public static final int SMALLTHAN = 0;
    public static final int LARGETHAN = 1;
    public static final int SMALLEQUAL = 2;
    public static final int LARGEEQUAL = 3;

    public IrRelOp(String s){
	opClass = RELOP;
	if (s.equals("<"))
	    opType = 0;
	else if (s.equals(">"))
	    opType = 1;
	else if (s.equals("<="))
	    opType = 2;
	else if (s.equals(">="))
	    opType = 3;
	else 
	    opType = -1;
    }
    
    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("relation operation: " + row + " " + col);	
    }
}