/*  FunctionDecl.java */

package ast;
import descriptor.*;

public class FunctionDecl extends MethodDecl{
    IrType irType;
    
    public FunctionDecl(IrType irType, IrId id, IrVarDeclList paramList, 
			IrBlock block){
	this.irType = irType;
	this.id = id;
	this.paramList = paramList;
	this.block = block;
	methodDescriptor = new MethodDescriptor(id.toString(), irType.type);
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Function declaration: " + row + " " + col);
	irType.printNode(indent + 1);
	id.printNode(indent + 1);
	for (int i = 0; i < paramList.size(); i++)
	    paramList.elementAt(i).printNode(indent + 1);
	block.printNode(indent + 1);
    }
}