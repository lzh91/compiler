/*  IrVarLocation.java */

package ast;

public class IrVarLocation extends IrLocation{
    IrId id;

    public IrVarLocation(IrId id){
	this.id = id;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("variable location: " + row + " " + col);
	id.printNode(indent + 1);
    }
}