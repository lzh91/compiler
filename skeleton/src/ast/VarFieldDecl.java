/*  VarFieldDecl.java */

package ast;
import descriptor.*;

public class VarFieldDecl extends FieldDecl{

    public VarFieldDecl(IrId id){
	this.id = id;
    }

    public int accept (Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Variable declaration: " + row + " " + col);
	id.printNode(indent+1);
    }
}