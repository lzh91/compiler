/*  MethodDeclList.java */

package ast;
import java.util.*;

public class MethodDeclList{
    private Vector list;
    
    public MethodDeclList(){
	list = new Vector();
    }

    public void addElement(MethodDecl m){
	list.addElement(m);
    }

    public MethodDecl elementAt(int i){
	return (MethodDecl)list.elementAt(i);
    }

    public int size(){
	return list.size();
    }
}