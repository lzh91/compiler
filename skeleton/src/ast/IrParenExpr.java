/*  IrParenExpr.java */

package ast;

public class IrParenExpr extends IrExpr{
    IrExpr e;

    public IrParenExpr(IrExpr e){
	this.e = e;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }
    
    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Parenthese expression: " + row + " " + col);
	e.printNode(indent + 1);
    }
}