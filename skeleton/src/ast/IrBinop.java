/*  IrBinop.java */

package ast;

public abstract class IrBinop extends IR{
    public int opType;
    public int opClass;
    public static final int ARITHOP = 0;
    public static final int RELOP = 1;
    public static final int EQOP = 2;
    public static final int CONDOP = 3;
}