/*  IrHexLiteral.java */

package ast;

public class IrHexLiteral extends IrIntLiteral{
    public IrHexLiteral(String s){
	value = Integer.parseInt(s.substring(2), 16);
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("heximal integer: " + row + " " + col + " " + value);
    }
}