/* Visitor.java */

package ast;

public interface Visitor{
    public int visit(IrClassDecl classDecl);
    public int visit(ArrayFieldDecl arrDecl);
    public int visit(IrType t);
    public int visit(IrId id);
    public int visit(IrBlock block);
    public int visit(FunctionDecl funcDecl);
    public int visit(IrIfThenStmt ifStmt);
    public int visit(IrIfThenElseStmt ifStmt);
    public int visit(IrArithOp arithOp);
    public int visit(IrArrElemLocation arrElem);
    public int visit(IrAssignOp assignOp);
    public int visit(IrAssignStmt assignStmt);
    public int visit(IrBinopExpr binopExpr);
    public int visit(IrBinop binop);
    public int visit(IrBlockStmt blockStmt);
    public int visit(IrBoolLiteral boolLit);
    public int visit(IrBreakStmt breakStmt);
    public int visit(IrCalloutExpr callExpr);
    public int visit(IrCalloutString callStr);
    public int visit(IrStringLit strLit);
    public int visit(IrCharLiteral charLit);
    public int visit(IrCondOp condOp);
    public int visit(IrContinueStmt contStmt);
    public int visit(IrDecLiteral decLit);
    public int visit(IrEqOp eqOp);
    public int visit(IrExMethodCall exCallout);
    public int visit(IrForStmt forStmt);
    public int visit(IrHexLiteral hexLit);
    public int visit(IrInMethodCall inCallout);
    public int visit(IrLiteralExpr litExpr);
    public int visit(IrLocationExpr locExpr);
    public int visit(IrMethodCallExpr callExpr);
    public int visit(IrMethodCallStmt callStmt);
    public int visit(IrNotExpr notExpr);
    public int visit(IrParenExpr expr);
    public int visit(IrRelOp relOp);
    public int visit(IrVarLocation varLoc);
    public int visit(ProcedureDecl procDecl);
    public int visit(VarFieldDecl fieldDecl);
    public int visit(IrUminusExpr expr);
    public int visit(IrMethodName mName);
    public int visit(IrLocalDecl localDecl);
    public int visit(IrParamDecl paramDecl);
    public int visit(IrReturnVarStmt returnStmt);
    public int visit(IrReturnVoidStmt returnStmt);
    public int visit(IrVarDecl varDecl);
}