/*  IrBlock.java */

package ast;

public class IrBlock extends IR{
    IrVarDeclList vars;
    IrStmtList statements;

    public IrBlock(IrVarDeclList vars, IrStmtList statements){
	this.vars = vars;
	this.statements = statements;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("block " + row + " " + col);
	for (int i = 0; i < vars.size(); i++)
	    vars.elementAt(i).printNode(indent + 1);
	for (int i = 0; i < statements.size(); i++)
	    statements.elementAt(i).printNode(indent + 1);
    }
}