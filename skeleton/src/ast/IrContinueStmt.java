/*  IrContinueStmt.java */

package ast;

public class IrContinueStmt extends IrStmt{
    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("continue statement: " + row + " " + col);
    }
}