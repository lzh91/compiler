/*  IrVarElemDeclList.java */

package ast;
import java.util.*;

public class IrVarElemDeclList{
    private Vector list;

    public IrVarElemDeclList(){
	list = new Vector();
    }

    public void addElement(IrVarElemDecl varDecl){
	list.add(varDecl);
    }

    public IrVarElemDecl elementAt(int i){
	return (IrVarElemDecl)list.elementAt(i);
    }

    public int size(){
	return list.size();
    }
}