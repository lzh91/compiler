/*  IrBinopExpr.java */

package ast;

public class IrBinopExpr extends IrExpr{
    IrBinop op;
    IrExpr e1;
    IrExpr e2;

    public IrBinopExpr(IrBinop op, IrExpr e1, IrExpr e2){
	this.op = op;
	this.e1 = e1;
	this.e2 = e2;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("binary operation expression: " + row 
			   + " " + col);
	e1.printNode(indent + 1);
	op.printNode(indent + 1);
	e2.printNode(indent + 1);
    }
}