/*  IrMethodCallStmt.java */

package ast;

public class IrMethodCallStmt extends IrStmt{
    IrMethodCall methodCall;

    public IrMethodCallStmt(IrMethodCall m){
	methodCall = m;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("method call statement: " + row + " " + col);
	methodCall.printNode(indent + 1);
    }
}