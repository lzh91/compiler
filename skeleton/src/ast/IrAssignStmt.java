/*  IrAssignStmt.java */

package ast;

public class IrAssignStmt extends IrStmt{
    IrLocation lhs;
    IrAssignOp op;
    IrExpr rhs;

    public IrAssignStmt(IrLocation lhs, IrAssignOp op, IrExpr rhs){
	this.lhs = lhs;
	this.op = op;
	this.rhs = rhs;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("assign statement: " + row + " " + col);
	lhs.printNode(indent + 1);
	op.printNode(indent + 1);
	rhs.printNode(indent + 1);
    }
}