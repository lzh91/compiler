/*  IrBlockStmt.java */

package ast;

public class IrBlockStmt extends IrStmt{
    IrBlock block;

    public IrBlockStmt(IrBlock b){
	block = b;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("block statement: " + row + " " + col);
	block.printNode(indent + 1);
    }
}