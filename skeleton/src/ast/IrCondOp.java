/*  IrCondOp.java */

package ast;

public class IrCondOp extends IrBinop{
    public static final int AND = 0;
    public static final int OR = 1;

    public IrCondOp(String s){
	opClass = CONDOP;
	if (s.equals("&&"))
	    opType = 0;
	else if (s.equals("||"))
	    opType = 1;
	else opType = -1;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("conditional operation: " + row + " " + col);
    }
}