/*  ArrayFieldDecl.java */

package ast;
import descriptor.*;

public class ArrayFieldDecl extends FieldDecl{
    IrIntLiteral arrSize;

    public ArrayFieldDecl(IrId id, IrIntLiteral arrSize){
	this.id = id;
	this.arrSize = arrSize;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i ++)
	    System.out.print(" ");
	System.out.println("Array field declaration: " + row + " " + col);
	id.printNode(indent + 1);
	arrSize.printNode(indent + 1);
    }
}