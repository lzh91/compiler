/*  IrStringLit.java */

package ast;

public class IrStringLit extends IR{
    public String strLit;

    public IrStringLit(String s){
	strLit = s;
    }
    
    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("String literal: " + row + " " + col);
    }
}