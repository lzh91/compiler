/*  IrNotExpr.java */

package ast;

public class IrNotExpr extends IrExpr{
    IrExpr e;
    public int countOfNot;

    public IrNotExpr(int c, IrExpr e){
	this.e = e;
	countOfNot = c;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("not expression: " + row + " " + col);
	e.printNode(indent + 1);
    }
}