/*  IrVarDeclList.java */

package ast;
import java.util.*;

public class IrVarDeclList{
    private Vector list;

    public IrVarDeclList(){
	list = new Vector();
    }

    public void addElement(IrVarDecl f){
	list.addElement(f);
    }

    public IrVarDecl elementAt(int i){
	return (IrVarDecl)list.elementAt(i);
    }

    public int size(){
	return list.size();
    }
}