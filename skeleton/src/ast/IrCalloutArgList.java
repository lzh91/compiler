/*  IrCalloutArgList.java */

package ast;
import java.util.*;

public class IrCalloutArgList{
    private Vector list;

    public IrCalloutArgList(){
	list = new Vector();
    }

    public void addElement(IrCalloutArg c){
	list.addElement(c);
    }

    public IrCalloutArg elementAt(int i){
	return (IrCalloutArg)list.elementAt(i);
    }

    public int size(){
	return list.size();
    }
}