/* IrCalloutString.java */

package ast;

public class IrCalloutString extends IrCalloutArg{
    IrStringLit s;

    public IrCalloutString(IrStringLit s){
	this.s = s;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("call out string: " + row + " " + col);
	s.printNode(indent + 1);
    }
}