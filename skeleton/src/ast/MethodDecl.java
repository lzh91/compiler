/*  MethodDecl.java */

package ast;
import descriptor.*;

public abstract class MethodDecl extends IR{
    protected IrId id;
    protected IrVarDeclList paramList;
    protected IrBlock block;
    protected Descriptor methodDescriptor;

    public String getMethodName(){
	return id.toString();
    }
}