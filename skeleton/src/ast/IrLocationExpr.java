/*  IrLocationExpr.java */

package ast;

public class IrLocationExpr extends IrExpr{
    IrLocation loc;

    public IrLocationExpr(IrLocation loc){
	this.loc = loc;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("location expression: " + row + " " + col);
	loc.printNode(indent + 1);
    }
}