/*  IrLiteralExpr.java */

package ast;

public class IrLiteralExpr extends IrExpr{
    IrLiteral lit;

    public IrLiteralExpr(IrLiteral l){
	lit = l;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("literal expression: " + row + " " + col);
	lit.printNode(indent + 1);
    }
}