/*  IrArrElemLocation.java */

package ast;

public class IrArrElemLocation extends IrLocation{
    IrId id;
    IrExpr index;

    public IrArrElemLocation(IrId id, IrExpr index){
	this.id = id;
	this.index = index;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Array element location: " + row + " " + col);
	id.printNode(indent + 1);
	index.printNode(indent + 1);
    }
}