/*  IrVarDecl.java */

package ast;
import descriptor.*;

public class IrVarDecl extends IR{
    IrType irType;
    IrVarElemDeclList vars;
    
    public IrVarDecl(IrType t, IrVarElemDeclList v){
	irType = t;
	vars = v;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Variable declaration: " + getRow() + " " + getCol());
	irType.printNode(indent + 1);
	for (int i = 0; i < vars.size(); i++)
	    vars.elementAt(i).printNode(indent + 1);
    }
}