/*  IrClassDecl.java */

package ast;
import descriptor.*;

public class IrClassDecl extends IR{
    IrVarDeclList fields;
    MethodDeclList methods;
    Descriptor classDescriptor;

    public IrClassDecl(IrVarDeclList f, MethodDeclList m){
	fields = f;
	methods = m;
    }

    public int accept(Visitor v){
	return v.visit(this);
    }

    public void printNode(int indent){
	for (int i = 0 ; i < indent; i++)
	    System.out.print(" ");
	System.out.println("Class Declaration: " + getRow() + " " + getCol());
	for (int i = 0; i < fields.size(); i++)
	    fields.elementAt(i).printNode(indent+1);
	for (int i = 0; i < methods.size(); i++)
	    methods.elementAt(i).printNode(indent+1);
    }
}