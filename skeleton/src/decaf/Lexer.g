header {package decaf;}

options 
{
  mangleLiteralPrefix = "TK_";
  language="Java";
}

class DecafScanner extends Lexer;
options 
{
  k=2;
}

tokens 
{
  "boolean";
  "break";
  "callout";
  "class";
  "continue";
  "else";
  "false";
  "for";
  "if";
  "int";
  "return";
  "true";
  "void";
}

LCURLY options { paraphrase = "{"; } : "{";
RCURLY options { paraphrase = "}"; } : "}";
LPAREN options { paraphrase = "("; } : "(";
RPAREN options { paraphrase = ")"; } : ")";
LSQUARE options { paraphrase = "["; } : "[";
RSQUARE options { paraphrase = "]"; } : "]";
ADD options { paraphrase = "+"; } : "+";
SUB options { paraphrase = "-"; } : "-";
MUL options { paraphrase = "*"; } : "*";
DIV options { paraphrase = "/"; } : "/";
MOD options { paraphrase = "%"; } : "%";
ASSIGN options { paraphrase = "="; } : "=";
PLUSASSIGN options { paraphrase = "+="; }: "+=";
MINUSASSIGN options { paraphrase = "-="; }: "-=";
LESSTHAN options { paraphrase = "<"; }: "<";
MORETHAN options { paraphrase = ">"; }: ">";
LESSEQUAL options { paraphrase = "<="; }: "<=";
MOREEQUAL options { paraphrase = ">="; }: ">=";
EQUAL options { paraphrase = "=="; }: "==";
NOTEQUAL options { paraphrase = "!="; }: "!=";
AND options { paraphrase = "&&"; }: "&&";
OR options { paraphrase = "||"; }: "||";
COMMA options { paraphrase = ","; }: ",";
SEMICOLON options { paraphrase = ";"; }: ";";
NOT options { paraphrase = "!"; }: "!";

ID options { paraphrase = "an identifier"; } : 
  (ALPHABET | '_')(ALPHABET | DIGIT | '_')*;

DECIMAL options { paraphrase = "decimal int"; } :
  (DIGIT)+;

HEXINT options { paraphrase = "hexical int"; } :
  "0x"('a'..'f' | 'A'..'F' | DIGIT)+;        

WS_ : (' ' | '\t' | '\r' | '\n' {newline();}) {_ttype = Token.SKIP; };

SL_COMMENT : "//" (~'\n')* '\n' {_ttype = Token.SKIP; newline (); };

CHAR : '\'' (VALIDCHARACTER) '\'';
STRING : '"' (VALIDCHARACTER)* '"';

protected
VALIDCHARACTER : (ESC|~('\''|'"'|'\\'|'\n'|'\t'|'\r'));

protected
ESC :  '\\' ('n'|'"'|'\\'|'\''|'t');

protected
DIGIT : ('0'..'9');

protected
ALPHABET : ('a'..'z'|'A'..'Z');
