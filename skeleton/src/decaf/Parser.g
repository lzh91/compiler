header {
package decaf;
import ast.*;
import symbol.*;
import environment.*;
import descriptor.*;
}

options
{
  mangleLiteralPrefix = "TK_";
  language="Java";
}

class DecafParser extends Parser;
options
{
  importVocab=DecafScanner;
  k=3;
  buildAST=true;
}

program returns [IrClassDecl classDecl]
{
    classDecl = null;
    IrVarDeclList fields = new IrVarDeclList();
    MethodDeclList methods = new MethodDeclList();
    IrVarDecl field;
    MethodDecl method;
}
    : cTok: TK_class ID LCURLY (field = field_decl
            {
                fields.addElement(field);
            }
        )* 
        (method = method_decl
            {
                methods.addElement(method);
            }
        )*  RCURLY EOF
        {
            classDecl = new IrClassDecl(fields, methods);
            classDecl.setRow(cTok.getLine());
            classDecl.setCol(cTok.getColumn());
        }
    ;

field_decl returns [IrVarDecl varDecl]
{
    varDecl = null;
    IrVarElemDeclList vars = new IrVarElemDeclList();
    IrIntLiteral lit1, lit2;
    IrType t;
}
    :t = type 
        (i1: ID
            {
                IrId id1 = new IrId(i1.getText());
                id1.setRow(i1.getLine());
                id1.setCol(i1.getColumn());
                IrVarElemDecl var1 = new VarFieldDecl(id1);
                var1.setRow(id1.getRow());
                var1.setCol(id1.getCol());
                vars.addElement(var1);
            }
        | i2: ID LSQUARE lit1 = int_literal RSQUARE
            {
                IrId id2 = new IrId(i2.getText());
                id2.setRow(i2.getLine());
                id2.setCol(i2.getColumn());
                IrVarElemDecl var2 = new ArrayFieldDecl(id2, lit1);
                var2.setRow(id2.getRow());
                var2.setCol(id2.getCol());
                vars.addElement(var2);
            }
        )
        (COMMA  i3: ID
            {
                IrId id3 = new IrId(i3.getText());
                id3.setRow(i3.getLine());
                id3.setCol(i3.getColumn());
                IrVarElemDecl var3 = new VarFieldDecl(id3);
                var3.setRow(id3.getRow());
                var3.setCol(id3.getCol());
                vars.addElement(var3);
            }
        | i4: ID LSQUARE lit2 = int_literal RSQUARE
            {
                IrId id4 = new IrId(i4.getText());
                id4.setRow(i4.getLine());
                id4.setCol(i4.getColumn());
                IrVarElemDecl var4 = new ArrayFieldDecl(id4, lit2);
                var4.setRow(id4.getRow());
                var4.setCol(id4.getCol());
                vars.addElement(var4);
            }
        )* 
        SEMICOLON
        {
            varDecl = new IrVarDecl(t, vars);
            varDecl.setRow(t.getRow());
            varDecl.setCol(t.getCol());
        }
    ;

method_decl returns [MethodDecl mDecl]
{
    mDecl = null;
    IrVarDeclList params = new IrVarDeclList();
    IrType t1 = null, t2, t3;
    IrBlock b;
}
    : (t1 = type | voidTok: TK_void) i1: ID
        {
            IrId mid = new IrId(i1.getText());
            mid.setRow(i1.getLine());
            mid.setCol(i1.getColumn());
        } 
        LPAREN (t2 = type i2: ID
            {
                IrId id1 = new IrId(i2.getText());
                id1.setRow(i2.getLine());
                id1.setCol(i2.getColumn());
                IrVarElemDecl var = new IrParamDecl(id1);
                var.setRow(id1.getRow());
                var.setCol(id1.getCol());
                IrVarElemDeclList vars = new IrVarElemDeclList();
                vars.addElement(var);
                IrVarDecl varDecl1 = new IrVarDecl(t2, vars);
                varDecl1.setRow(t2.getRow());
                varDecl1.setCol(t2.getCol());
                params.addElement(varDecl1);
            }
            (COMMA t3 = type i3: ID
                {
                    IrId id2 = new IrId(i3.getText());
                    id2.setRow(i3.getLine());
                    id2.setCol(i3.getColumn());
                    IrVarElemDecl var2 = new IrParamDecl(id2);
                    var2.setRow(id2.getRow());
                    var2.setCol(id2.getCol());
                    IrVarElemDeclList vars2 = new IrVarElemDeclList();
                    vars2.addElement(var2);
                    IrVarDecl varDecl2 = new IrVarDecl(t3, vars2);
                    varDecl2.setRow(t3.getRow());
                    varDecl2.setCol(t3.getCol());
                    params.addElement(varDecl2);
                }
            )*)? RPAREN 
        b = block
        {
            if (t1 == null){
                mDecl = new ProcedureDecl(mid, params, b);
                mDecl.setRow(voidTok.getLine());
                mDecl.setCol(voidTok.getColumn());
            } else{
                mDecl = new FunctionDecl(t1, mid, params, b);
                mDecl.setRow(t1.getRow());
                mDecl.setCol(t1.getCol());
            }
        }
    ;

block returns [IrBlock b]
{
    b = null;
    IrVarDeclList vars = new IrVarDeclList();
    IrStmtList stmts = new IrStmtList();
    IrVarDecl var;
    IrStmt stmt;
}
    : l: LCURLY (var = var_decl
            {
                vars.addElement(var);
            }
        )* (stmt = statement
            {
                stmts.addElement(stmt);
            }
        )* RCURLY
        {
            b = new IrBlock(vars, stmts);
            b.setRow(l.getLine());
            b.setCol(l.getColumn());
        }
    ;

var_decl returns [IrVarDecl varDecl]
{
    varDecl = null;
    IrType t;
    IrVarElemDeclList vars = new IrVarElemDeclList();
}
    : t = type i1: ID 
        {
            IrId id = new IrId(i1.getText());
            id.setRow(i1.getLine());
            id.setCol(i1.getColumn());
            IrVarElemDecl var = new IrLocalDecl(id);
            var.setRow(id.getRow());
            var.setCol(id.getCol());
            vars.addElement(var);
        }
        (COMMA i2: ID
            {
                IrId id2 = new IrId(i2.getText());
                id2.setRow(i2.getLine());
                id2.setCol(i2.getColumn());
                IrVarElemDecl var2 = new IrLocalDecl(id2);
                var2.setRow(id2.getRow());
                var2.setCol(id2.getCol());
                vars.addElement(var2);
            }
        )* SEMICOLON
        {
            varDecl = new IrVarDecl(t, vars);
            varDecl.setRow(t.getRow());
            varDecl.setCol(t.getCol());
        }
    ; 

type returns [IrType t]
{
    t = null;
}
    : i : TK_int 
        {
            t = new IrType(i.getText());
            t.setRow(i.getLine());
            t.setCol(i.getColumn());
        }
    | b : TK_boolean
        {
            t = new IrType(b.getText()); 
            t.setRow(b.getLine());
            t.setCol(b.getColumn());
        }
    ;

statement returns [IrStmt stmt]
{
    stmt = null;
    IrLocation loc;
    IrAssignOp op;
    IrExpr e;
    IrMethodCall m;
    IrExpr condition;
    IrBlock thenStmt;
    IrBlock elseStmt;
    IrExpr initExpr;
    IrExpr finalExpr;
    IrBlock forBlock;
    IrExpr returnExpr;
    IrBlock b;
}
    : loc = location op = assign_op e = expr SEMICOLON
        {
            stmt = new IrAssignStmt(loc, op, e);
            stmt.setRow(loc.getRow());
            stmt.setCol(loc.getCol());
        }
    | m = method_call SEMICOLON
        {
            stmt = new IrMethodCallStmt(m);
            stmt.setRow(m.getRow());
            stmt.setCol(m.getCol());
        }
    | ifTok: TK_if LPAREN condition = expr RPAREN thenStmt = block
        {
            stmt = new IrIfThenStmt(condition, thenStmt);
        }
        (TK_else elseStmt = block
            {
                stmt = new IrIfThenElseStmt(condition, thenStmt, elseStmt);
            }
        )?
        {
            stmt.setRow(ifTok.getLine());
            stmt.setCol(ifTok.getColumn());
        }
    | forTok: TK_for id: ID ASSIGN initExpr = expr COMMA finalExpr = expr forBlock = block
        {
            IrId forId = new IrId(id.getText());
            forId.setRow(id.getLine());
            forId.setCol(id.getColumn());
            stmt = new IrForStmt(forId, initExpr, finalExpr, forBlock);
            stmt.setRow(forTok.getLine());
            stmt.setCol(forTok.getColumn());
        }
    | retTok: TK_return 
        {
            stmt = new IrReturnVoidStmt();
        }
        (returnExpr = expr
            {
                stmt = new IrReturnVarStmt(returnExpr);
            }
        )? SEMICOLON
        {
            stmt.setRow(retTok.getLine());
            stmt.setCol(retTok.getColumn());
        }
    | br: TK_break SEMICOLON
        {
            stmt = new IrBreakStmt();
            stmt.setRow(br.getLine());
            stmt.setCol(br.getColumn());
        }
    | cont: TK_continue SEMICOLON
        {
            stmt = new IrContinueStmt();
            stmt.setRow(cont.getLine());
            stmt.setCol(cont.getColumn());
        }
    | b = block
        {
            stmt = new IrBlockStmt(b);
            stmt.setRow(b.getRow());
            stmt.setCol(b.getCol());
        }
    ;

assign_op returns [IrAssignOp op]
{
    op = null;
}
    : a: ASSIGN
        {
            op = new IrAssignOp(a.getText());
            op.setRow(a.getLine());
            op.setCol(a.getColumn());
        }
    | p: PLUSASSIGN
        {
            op = new IrAssignOp(p.getText());
            op.setRow(p.getLine());
            op.setCol(p.getColumn());
        }
    | m: MINUSASSIGN
        {
            op = new IrAssignOp(m.getText());
            op.setRow(m.getLine());
            op.setCol(m.getColumn());
        }
    ;

method_call returns [IrMethodCall methodCall]
{
    methodCall = null;
    IrMethodName mName;
    IrExprList exprs = new IrExprList();
    IrCalloutArgList args = new IrCalloutArgList();
    IrExpr e1, e2;
    IrCalloutArg c1, c2;
}
    : mName = method_name LPAREN (e1 = expr
            {
                exprs.addElement(e1);
            }
            (COMMA e2 = expr
                {
                    exprs.addElement(e2);
                }
            )*)? RPAREN
        {
            methodCall = new IrInMethodCall(mName, exprs);
            methodCall.setRow(mName.getRow());
            methodCall.setCol(mName.getCol());
        }
    | t: TK_callout LPAREN s: STRING (COMMA c1 = callout_arg
            {
                args.addElement(c1);
            }
            (COMMA c2 = callout_arg
                {
                    args.addElement(c2);
                }
            )*)? RPAREN
        {
            IrStringLit strLit = new IrStringLit(s.getText());
            strLit.setRow(s.getLine());
            strLit.setCol(s.getColumn());
            methodCall = new IrExMethodCall(strLit, args);
            methodCall.setRow(t.getLine());
            methodCall.setCol(t.getColumn());
        }
    ;

method_name returns [IrMethodName mName]
{
    mName = null;
}
    : i: ID
        {
            IrId id = new IrId(i.getText());
            id.setRow(i.getLine());
            id.setCol(i.getColumn());
            mName = new IrMethodName(id);
            mName.setRow(id.getRow());
            mName.setCol(id.getCol());
        }
    ;

location returns [IrLocation loc]
{
    loc = null;
    IrExpr e;
}
    : i1: ID
        {
            IrId id = new IrId(i1.getText());
            id.setRow(i1.getLine());
            id.setCol(i1.getColumn());
            loc = new IrVarLocation(id);
            loc.setRow(id.getRow());
            loc.setCol(id.getCol());
        }
    | i2: ID LSQUARE e = expr RSQUARE
        {
            IrId id = new IrId(i2.getText());
            id.setRow(i2.getLine());
            id.setCol(i2.getColumn());
            loc = new IrArrElemLocation(id, e);
            loc.setRow(id.getRow());
            loc.setCol(id.getCol());
        }
    ;

expr returns [IrExpr expr]
{
    expr = null;
    IrExpr e1, e2;
}
    : e1 = or_expr 
        {
            expr = e1;
        }
        (or: OR e2 = or_expr
            {
                IrBinop op = new IrCondOp(or.getText());
                op.setRow(or.getLine());
                op.setCol(or.getColumn());
                IrExpr tmpExpr = expr;
                expr = new IrBinopExpr(op, expr, e2);
                expr.setRow(tmpExpr.getRow());
                expr.setCol(tmpExpr.getCol());
            }
        )*
    ;

or_expr returns [IrExpr expr]
{
    expr = null;
    IrExpr e1, e2;
}
    : e1 = and_expr 
        {
            expr = e1;
        }
        (and: AND e2 = and_expr
            {
                IrBinop op = new IrCondOp(and.getText());
                op.setRow(and.getLine());
                op.setCol(and.getColumn());
                IrExpr tmpExpr = expr;
                expr = new IrBinopExpr(op, expr, e2);
                expr.setRow(tmpExpr.getRow());
                expr.setCol(tmpExpr.getCol());
            }
        )*
    ;

and_expr returns [IrExpr expr]
{
    expr = null;
    IrBinop op;
    IrExpr e1, e2;
}
    : e1 = eq_expr
        {
            expr = e1;
        }
        (op = eq_op e2 = eq_expr
            {
                IrExpr tmpExpr = expr;
                expr = new IrBinopExpr(op, expr, e2);
                expr.setRow(tmpExpr.getRow());
                expr.setCol(tmpExpr.getCol());
            }
        )*                         
    ;

eq_expr returns [IrExpr expr]
{
    expr = null;
    IrBinop op;
    IrExpr e1, e2;
}
    : e1 = rel_expr
        {
            expr = e1;
        }
        (op = rel_op e2 = rel_expr
            {
                IrExpr tmpExpr = expr;
                expr = new IrBinopExpr(op, expr, e2);
                expr.setRow(tmpExpr.getRow());
                expr.setCol(tmpExpr.getCol());
            }
        )*
    ;

rel_expr returns [IrExpr expr]
{
    expr = null;
    IrBinop op;
    IrExpr e1, e2;
}
    : e1 = addmin_expr 
        {
            expr = e1;
        }
        (op = addmin_op e2 = addmin_expr
            {
                IrExpr tmpExpr = expr;
                expr = new IrBinopExpr(op, expr, e2);
                expr.setRow(tmpExpr.getRow());
                expr.setCol(tmpExpr.getCol());
            }
        )*
    ;

addmin_expr returns [IrExpr expr]
{
    expr = null;
    IrBinop op;
    IrExpr e1, e2;
}
    : e1 = muldiv_expr
        {
            expr = e1;
        }
        (op = muldiv_op e2 = muldiv_expr
            {
                IrExpr tmpExpr = expr;
                expr = new IrBinopExpr(op, expr, e2);
                expr.setRow(tmpExpr.getRow());
                expr.setCol(tmpExpr.getCol());
            }
        )*
    ;

muldiv_expr returns [IrExpr expr]
{
    expr = null;
    int count = 0;
    int row = 0, col = 0;
    IrExpr e1;
}
    : (n: NOT
            {
                if (count == 0){
                    row = n.getLine();
                    col = n.getColumn();
                }
                count ++;
            }
        )* e1 = term
        {
            expr = new IrNotExpr(count, e1);
            if (count == 0){
                expr.setRow(e1.getRow());
                expr.setCol(e1.getCol());
            } else{
                expr.setRow(row);
                expr.setCol(col);
            }
        }
    ;

term returns [IrExpr expr]
{
    expr = null;
    int count = 0;
    int row = 0, col = 0;
    IrExpr e1;
}
    : (s: SUB
            {
                if (count == 0){
                    row = s.getLine();
                    col = s.getColumn();
                }
                count++;
            }
        )* e1 = atom
        {
            expr = new IrUminusExpr(count, e1);
            if (count == 0){
                expr.setRow(e1.getRow());
                expr.setCol(e1.getCol());
            } else{
                expr.setRow(row);
                expr.setCol(col);
            }
        }
    ;

atom returns [IrExpr expr]
{
    expr = null;
    IrExpr e;
    IrLiteral l;
    IrLocation loc;
    IrMethodCall m;
}
    : LPAREN e = expr RPAREN
        {
            expr = new IrParenExpr(e);
            expr.setRow(e.getRow());
            expr.setCol(e.getCol());
        }
    | l = literal
        {
            expr = new IrLiteralExpr(l);
            expr.setRow(l.getRow());
            expr.setCol(l.getCol());
        }
    | loc = location
        {
            expr = new IrLocationExpr(loc);
            expr.setRow(loc.getRow());
            expr.setCol(loc.getCol());
        }
    | m = method_call
        {
            expr = new IrMethodCallExpr(m);
            expr.setRow(m.getRow());
            expr.setCol(m.getCol());
        }
    ;

addmin_op returns [IrArithOp arithOp]
{
    arithOp = null;
}
    : add: ADD
        {
            arithOp = new IrArithOp(add.getText());
            arithOp.setRow(add.getLine());
            arithOp.setCol(add.getColumn());
        }
    | sub: SUB
        {
            arithOp = new IrArithOp(sub.getText());
            arithOp.setRow(sub.getLine());
            arithOp.setCol(sub.getColumn());
        }
    ;

muldiv_op returns [IrArithOp arithOp]
{
    arithOp = null;
}
    : mul: MUL
        {
           arithOp = new IrArithOp(mul.getText()); 
           arithOp.setRow(mul.getLine());
           arithOp.setCol(mul.getColumn());
        }
    | div: DIV
        {
            arithOp = new IrArithOp(div.getText());
            arithOp.setRow(div.getLine());
            arithOp.setCol(div.getColumn());
        }
    | mod: MOD
        {
            arithOp = new IrArithOp(mod.getText());
            arithOp.setRow(mod.getLine());
            arithOp.setCol(mod.getColumn());
        }
    ;

callout_arg returns [IrCalloutArg callout]
{
    callout = null;
    IrExpr e;
}
    : s: STRING
        {
            IrStringLit strLit;
            strLit = new IrStringLit(s.getText());
            strLit.setRow(s.getLine());
            strLit.setCol(s.getColumn());
            callout = new IrCalloutString(strLit);
            callout.setRow(strLit.getRow());
            callout.setCol(strLit.getCol());
        }
    | e = expr
        {
            callout = new IrCalloutExpr(e);
            callout.setRow(e.getRow());
            callout.setCol(e.getCol());
        }
    ;

rel_op returns [IrRelOp relOp]
{
    relOp = null;
}
    : lt: LESSTHAN
        {
            relOp = new IrRelOp(lt.getText());
            relOp.setRow(lt.getLine());
            relOp.setCol(lt.getColumn());
        }
    | mt: MORETHAN
        {
            relOp = new IrRelOp(mt.getText());
            relOp.setRow(mt.getLine());
            relOp.setCol(mt.getColumn());
        }
    | le: LESSEQUAL
        {
            relOp = new IrRelOp(le.getText());
            relOp.setRow(le.getLine());
            relOp.setCol(le.getColumn());
        }
    | me: MOREEQUAL
        {
            relOp = new IrRelOp(me.getText());
            relOp.setRow(me.getLine());
            relOp.setCol(me.getColumn());
        }
    ;

eq_op returns [IrEqOp eqOp]
{
    eqOp = null;
}
    : e: EQUAL
        {
            eqOp = new IrEqOp(e.getText());
            eqOp.setRow(e.getLine());
            eqOp.setCol(e.getColumn());
        }
    | n: NOTEQUAL
        {
            eqOp = new IrEqOp(n.getText());
            eqOp.setRow(n.getLine());
            eqOp.setCol(n.getColumn());
        }
    ;

literal returns [IrLiteral lit]
{
    lit = null;
    IrIntLiteral i;
    IrBoolLiteral b;
}
    : i = int_literal
        {
            lit = i;
            lit.setRow(i.getRow());
            lit.setCol(i.getCol());
        }
    | c: CHAR
        {
            lit = new IrCharLiteral(c.getText());
            lit.setRow(c.getLine());
            lit.setCol(c.getColumn());
        }
    | b = bool_literal
        {
            lit = b;
            lit.setRow(b.getRow());
            lit.setCol(b.getCol());
        }
    ;

int_literal returns[IrIntLiteral intLit]
{
    intLit = null;
}
    : d: DECIMAL
        {
            intLit = new IrDecLiteral(d.getText());
            intLit.setRow(d.getLine());
            intLit.setCol(d.getColumn());
        }
    | h: HEXINT
        {
            intLit = new IrHexLiteral(h.getText());
            intLit.setRow(h.getLine());
            intLit.setCol(h.getColumn());
        }
    ;

bool_literal returns [IrBoolLiteral boolLit]
{
    boolLit = null;
}
    : t: TK_true
        {
            boolLit = new IrBoolLiteral(t.getText());
            boolLit.setRow(t.getLine());
            boolLit.setCol(t.getColumn());
        }
    | f: TK_false
        {
            boolLit = new IrBoolLiteral(f.getText());
            boolLit.setRow(f.getLine());
            boolLit.setCol(f.getColumn());
        }
    ;
