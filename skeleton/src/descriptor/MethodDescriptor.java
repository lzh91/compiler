/*  MethodDescriptor.java */

package descriptor;
import java.util.*;
import symbol.*;

public class MethodDescriptor extends Descriptor{
    private Vector<Binding> params = new Vector<Binding>();
    private int returnType;

    public MethodDescriptor(String name, int t){
	descripType = Descriptor.METHOD;
	this.name = name;
	returnType = t;
    }

    public void addParam(Symbol s, int t){
	params.add(new Binding(s, t));
    }

    public int paramSize(){
	return params.size();
    }

    public Binding paramElementAt(int i){
	return params.elementAt(i);
    }

    public int getReturnType(){
	return returnType;
    }

    public int getDescriptorType(){
	return descripType;
    }
    
    public String getName(){
	return name;
    }
}