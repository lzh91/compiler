/*  Binding.java */

package descriptor;
import symbol.*;

public class Binding{
    public Symbol symbol;
    public int type;

    public Binding(Symbol s, int t){
	symbol = s;
	type = t;
    }
}