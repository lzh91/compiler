/*  ClassDescriptor.java */

package descriptor;
import java.util.*;
import symbol.*;

public class ClassDescriptor extends Descriptor{
    private Vector<Binding> fields = new Vector<Binding>();
    private Vector<Binding> methods = new Vector<Binding>();

    public ClassDescriptor(String s){
	descripType = Descriptor.CLASS;
	name = s;
    }

    public void addField(Symbol s, int type){
	fields.addElement(new Binding(s, type));
    }

    public Binding fieldElementAt(int i){
	return fields.elementAt(i);
    }

    public int fieldSize(){
	return fields.size();
    }
    
    public void addMethodElement(Symbol s, int type){
	methods.addElement(new Binding(s, type));
    }

    public Binding methodElementAt(int i){
	return methods.elementAt(i);
    }

    public int methodSize(){
	return methods.size();
    }
}