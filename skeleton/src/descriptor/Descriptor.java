/*  Descriptor.java */

package descriptor;

public abstract class Descriptor{
    public final static int CLASS = 0;
    public final static int FIELD = 1;
    public final static int LOCAL = 2;
    public final static int METHOD = 3;
    public final static int PARAM = 4;

    public int descripType;
    protected String name;
}