/*  ParamDescriptor.java */

package descriptor;

public class ParamDescriptor extends Descriptor{
    private int type;

    public ParamDescriptor(String s, int t){
	name = s;
	type = t;
	descripType = Descriptor.PARAM;
    }

    public int getType(){
	return type;
    }

    public String getName(){
	return name;
    }
}