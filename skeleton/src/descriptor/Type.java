/*  Type.java */

package descriptor;

public class Type{
    public final static int ERROR = -1;
    public final static int VOID = 0;
    public final static int INTEGER = 1;
    public final static int BOOLEAN = 2;
    public final static int INTARRAY = 3;
    public final static int BOOLARRAY = 4;
    public final static int CHAR = 5;
    public final static int STRING = 6;
}