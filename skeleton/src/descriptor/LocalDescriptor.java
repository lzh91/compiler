/*  LocalDescriptor.java */

package descriptor;

public class LocalDescriptor extends Descriptor{
    private int type;

    public LocalDescriptor(String s, int t){
	name = s;
	type = t;
	descripType = Descriptor.LOCAL;
    }

    public int getType(){
	return type;
    }

    public String getName(){
	return name;
    }
}