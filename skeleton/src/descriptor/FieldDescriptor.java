/*  FieldDescriptor.java */

package descriptor;

public class FieldDescriptor extends Descriptor{
    private int type;

    public FieldDescriptor(String s, int t){
	name = s;
	type = t;
	descripType = Descriptor.FIELD;
    }

    public int getType(){
	return type;
    }

    public String getName(){
	return name;
    }
}