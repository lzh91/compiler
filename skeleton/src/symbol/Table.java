package symbol;
import java.util.*;
import environment.*;
import descriptor.*;

public class Table{
    private Dictionary<Symbol, LinkedList<Descriptor>> dict;
    private Stack<Symbol> auxStack;
    private String nameSpace;

    public String getNameSpace(){
	return nameSpace;
    }

    public void setNameSpace(String s){
	nameSpace = s;
    }

    public Table(){
	dict = new Hashtable<Symbol, LinkedList<Descriptor>>();
	auxStack = new Stack<Symbol>();
    }

    public void put(Symbol key, Descriptor value){
	LinkedList<Descriptor> list = dict.get(key);
	if (list == null){
	    list = new LinkedList<Descriptor>();	    
	}
	list.addFirst(value);
	dict.put(key, list);
	auxStack.push(key);
    }

    public Descriptor get(Symbol key){
	LinkedList<Descriptor> list = dict.get(key);
	if (list == null)
	    return null;
	return list.peekFirst();
    }

    public void beginScope(){
	auxStack.push(null);
    }

    public void endScope(){
	while (!auxStack.empty()){
	    Symbol key = auxStack.pop();
	    if (key == null)
		break;
	    LinkedList<Descriptor> list = dict.get(key);
	    list.removeFirst();
	}
    }

    public Enumeration<Symbol> keys(){
	return dict.keys();
    }
}