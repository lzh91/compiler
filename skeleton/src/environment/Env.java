/*  Env.java */

package environment;
import symbol.*;
import java.util.*;

public class Env{
    private static Vector list
	= new Vector();

    public static void addElement(Table t){
	list.addElement(t);
    }

    public static Table elementAt(int i){
	return (Table)list.elementAt(i);
    }

    public static int size(){
	return list.size();
    }
}