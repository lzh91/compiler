/*  Scope.java */

package environment;

public class Scope{
    private static String nameSpace;
    private static String methodName;
    private static boolean isLoop = false;

    public static void setIsLoop(boolean flag){
	isLoop = flag;
    }

    public static boolean getIsLoop(){
	return isLoop;
    }

    public static void setNameSpace(String s){
	nameSpace = s;
    }

    public static String getNameSpace(){
	return nameSpace;
    }

    public static void setMethodName(String s){
	methodName = s;
    }

    public static String getMethodName(){
	return methodName;
    }
}